package cn.ydxiaoshuai.common.api.vo.oss;

import com.qcloud.cos.model.PutObjectResult;
import lombok.Data;

/**
 * @author 小帅丶
 * @className TencentPutObjectResult
 * @Description OSS对象
 * @Date 2020/5/14-18:14
 **/
@Data
public class TencentPutObjectResult extends PutObjectResult {

    /** 图片地址 **/
    private String img_url;
    /** 图片名称 **/
    private String img_name;
}
