package cn.ydxiaoshuai.modules.quartz.job;

import cn.ydxiaoshuai.common.factory.BDFactory;
import cn.ydxiaoshuai.common.sdkpro.AipFacePro;
import cn.ydxiaoshuai.common.util.RedisUtil;
import cn.ydxiaoshuai.modules.conts.RedisBizzKey;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * @author 小帅丶
 * @className StartMethodRunner
 * @Description 初始加载方法
 * @Date 2020/4/10-15:05
 **/
@Component
@Slf4j
public class AStartMethodRunner implements ApplicationRunner {
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private ToutiaoJob toutiaoJob;
    @Autowired
    private WeiXinAccessTokenJob weiXinAccessTokenJob;
    @Override
    public void run(ApplicationArguments args) {
          log.info("初始化一些变量，请先设置相关参数再开启下面注释的代码");
//        log.info("单例加载百度SDK");
//        AipFacePro aipFacePro = BDFactory.getAipFacePro();
//        aipFacePro.detect("http://aip.bdstatic.com/portal-pc-node/dist/1586441726384/images/technology/face/detect/demo-card-2.jpg", "URL", null);
//        log.info("初始化头条AccessToken");
//        toutiaoJob.getAccessTokenTouTiao();
//        log.info("初始化微信小程序AccessToken");
//        weiXinAccessTokenJob.refreshAccessToken();
//        log.info("初始化是否开启图片违规检测");
//        redisUtil.set(RedisBizzKey.IS_CHECK_IMG, RedisBizzKey.IS_CHECK_IMG_YES);
    }
}
