package cn.ydxiaoshuai.modules.service;

import cn.ydxiaoshuai.modules.entity.LiteApiLog;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * @Description: API日志记录表
 * @Author: 小帅丶
 * @Date:   2020-04-30
 * @Version: V1.0
 */
public interface ILiteApiLogService extends IService<LiteApiLog> {
    List<Map<String, Object>> getAllCount();

    List<Map<String, Object>> getUserRank();

    List<Map<String, Object>> getAllCountToDay();

    List<Map<String, Object>> getUserRankToDay();
}
