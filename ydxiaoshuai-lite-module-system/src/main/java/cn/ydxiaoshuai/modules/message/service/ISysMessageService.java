package cn.ydxiaoshuai.modules.message.service;

import cn.ydxiaoshuai.common.system.base.service.JeecgService;
import cn.ydxiaoshuai.modules.message.entity.SysMessage;

/**
 * @Description: 消息
 * @Author: 小帅丶
 * @Date:  2019-04-09
 * @Version: V1.0
 */
public interface ISysMessageService extends JeecgService<SysMessage> {

}
