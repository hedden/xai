package cn.ydxiaoshuai.modules.logoexamine.service.impl;

import cn.ydxiaoshuai.modules.logoexamine.entity.LogoExamineRecord;
import cn.ydxiaoshuai.modules.logoexamine.mapper.LogoExamineRecordMapper;
import cn.ydxiaoshuai.modules.logoexamine.service.ILogoExamineRecordService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: LOGO自定义上传审核记录表
 * @Author: 小帅丶
 * @Date:   2020-05-14
 * @Version: V1.0
 */
@Service
public class LogoExamineRecordServiceImpl extends ServiceImpl<LogoExamineRecordMapper, LogoExamineRecord> implements ILogoExamineRecordService {

}
