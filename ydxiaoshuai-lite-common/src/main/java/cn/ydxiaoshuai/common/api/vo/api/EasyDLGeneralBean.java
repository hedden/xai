package cn.ydxiaoshuai.common.api.vo.api;

import lombok.Data;

/**
 * @author 小帅丶
 * @className EasyDLGeneralBean
 * @Description EasyDLGeneralBean
 * @Date 2020/3/12-11:20
 **/
@Data
public class EasyDLGeneralBean extends BaseBean {

    private EasyDLChinesResponseBean data;

    public EasyDLGeneralBean success(String msg,String msg_zh, EasyDLChinesResponseBean data) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 200;
        this.data = data;
        return this;
    }
    public EasyDLGeneralBean fail(String msg,String msg_zh, Integer code) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = code;
        return this;
    }
    public EasyDLGeneralBean error(String msg,String msg_zh) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 500;
        return this;
    }
}
