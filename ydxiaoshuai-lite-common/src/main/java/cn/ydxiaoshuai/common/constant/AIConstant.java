package cn.ydxiaoshuai.common.constant;

/**
 * @author 小帅丶
 * @className AIConstant
 * @Description 应用信息 也不会存在频繁替换 就写死在代码中了
 * @Date 2020/5/14-17:17
 **/
public interface AIConstant {
    /**人脸应用信息**/
    public static String BD_FACE_APPID = "";
    public static String BD_FACE_APIKEY = "";
    public static String BD_FACE_SECRETKEY = "";
    /**人脸特效应用信息**/
    public static String BD_FACE_MED_BEAUTY_APPID = "";
    public static String BD_FACE_MED_BEAUTY_APIKEY = "";
    public static String BD_FACE_MED_BEAUTY_SECRETKEY = "";
    /**文字识别应用信息**/
    public static String BD_OCR_APPID = "";
    public static String BD_OCR_APIKEY = "";
    public static String BD_OCR_SECRETKEY = "";
    /**图像识别应用信息**/
    public static String BD_ICR_APPID = "";
    public static String BD_ICR_APIKEY = "";
    public static String BD_ICR_SECRETKEY = "";
    /**图像效果增强应用信息**/
    public static String BD_IMAGEPROCESS_APPID = "";
    public static String BD_IMAGEPROCESS_APIKEY = "";
    public static String BD_IMAGEPROCESS_SECRETKEY = "";
    /**人体分析应用信息**/
    public static String BD_BODY_APPID = "";
    public static String BD_BODY_APIKEY = "";
    public static String BD_BODY_SECRETKEY = "";
}
