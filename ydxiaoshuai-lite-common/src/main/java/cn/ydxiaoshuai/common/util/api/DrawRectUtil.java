package cn.ydxiaoshuai.common.util.api;

import cn.ydxiaoshuai.common.api.vo.api.*;
import com.baidu.aip.util.Base64Util;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author 小帅丶
 * @className DrawRectUtil
 * @Description 图片处理工具类
 * @Date 2020/4/10-14:55
 **/
public class DrawRectUtil {
    /**
     * @Author 小帅丶
     * @Description 给图片增加框
     * @Date  2020年4月10日14:57:47
     * @param imageBytes 图片
     * @param bean 包含坐标的对象 x-left y-top width-width height-height
     *             width = right-left
     *             height = bootom-top
     * @return String 处理后的base64
     **/
    public static String getReactByAcnespotmole(byte[] imageBytes, MedicalBeautyAcnespotmoleResponse bean) throws Exception{
        ByteArrayInputStream bais = new ByteArrayInputStream(imageBytes);
        BufferedImage image = ImageIO.read(bais);
        Graphics g = image.getGraphics();
        Graphics2D g2d = (Graphics2D) g;
        java.util.List<MedicalBeautyAcnespotmoleResponse.ResultBean.FaceListBean.AcneListBean> acne_list = bean.getResult().getFace_list().get(0).getAcne_list();
        java.util.List<MedicalBeautyAcnespotmoleResponse.ResultBean.FaceListBean.SpeckleListBean> speckle_list = bean.getResult().getFace_list().get(0).getSpeckle_list();
        List<MedicalBeautyAcnespotmoleResponse.ResultBean.FaceListBean.MoleListBean> mole_list = bean.getResult().getFace_list().get(0).getMole_list();
        Font font = new Font("微软雅黑", Font.ITALIC, 16);
        //痘信息
        for (int i = 0; i < acne_list.size(); i++) {
            g2d.setColor(new Color(0, 129, 255));
            int width  = (int)acne_list.get(i).getRight()-(int)acne_list.get(i).getLeft();
            int height = (int)acne_list.get(i).getBottom()-(int)acne_list.get(i).getTop();
            g2d.setStroke(new BasicStroke(2.0f));
            g2d.drawRect((int)acne_list.get(i).getLeft(), (int)acne_list.get(i).getTop(),width, height);
        }
        //斑信息
        for (int i = 0; i < speckle_list.size(); i++) {
            g2d.setColor(new Color(57, 181, 74));
            int speckleWidth  = (int)speckle_list.get(i).getRight()-(int)speckle_list.get(i).getLeft();
            int speckleHeight = (int)speckle_list.get(i).getBottom()-(int)speckle_list.get(i).getTop();
            g2d.setStroke(new BasicStroke(2.0f));
            g2d.drawRect((int)speckle_list.get(i).getLeft(), (int)speckle_list.get(i).getTop(),speckleWidth, speckleHeight);
        }
        //痣信息
        for (int i = 0; i < mole_list.size(); i++) {
            g2d.setColor(new Color(229, 77, 66));
            int width  = (int)mole_list.get(i).getRight()-(int)mole_list.get(i).getLeft();
            int height = (int)mole_list.get(i).getBottom()-(int)mole_list.get(i).getTop();
            g2d.setStroke(new BasicStroke(2.0f));
            g2d.drawRect((int)mole_list.get(i).getLeft(), (int)mole_list.get(i).getTop(),width, height);
        }
        g2d.setFont(font);
        g2d.setColor(Color.white);
        //g.drawString(acne_list.size()+"个痘,"+speckle_list.size()+"个斑,"+mole_list.size()+"个痣",0,20);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", outputStream);
        String base64Img = Base64Util.encode(outputStream.toByteArray());
        return base64Img;
    }
    /**
     * @Author 小帅丶
     * @Description 给图片增加框
     * @Date  2020年4月14日14:29:38
     * @param imageBytes 图片
     * @param bean 包含坐标的对象 x-left y-top width-width height-height
     * @return String 处理后的base64
     **/
    public static String getReactBySkinSmooth(byte[] imageBytes, MedicalBeautySkinSmoothResponse bean) throws Exception{
        ByteArrayInputStream bais = new ByteArrayInputStream(imageBytes);
        BufferedImage image = ImageIO.read(bais);
        Graphics g = image.getGraphics();
        Graphics2D g2d = (Graphics2D) g;
        Font font = new Font("微软雅黑", Font.ITALIC, 16);
        if (bean.getResult().getFace_num()>0) {
            for (int i = 0; i < bean.getResult().getFace_list().size(); i++) {
                int left = (int)bean.getResult().getFace_list().get(i).getLocation().getLeft();
                int top = (int)bean.getResult().getFace_list().get(i).getLocation().getTop();
                int width = bean.getResult().getFace_list().get(i).getLocation().getWidth();
                int height =bean.getResult().getFace_list().get(i).getLocation().getHeight();
                g2d.setColor(new Color(0, 129, 255));
                g2d.setStroke(new BasicStroke(2.0f));
                g2d.drawRect(left,top,width,height);
            }
        }
        g2d.setFont(font);
        g2d.setColor(Color.white);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", outputStream);
        String base64Img = Base64Util.encode(outputStream.toByteArray());
        return base64Img;
    }

    /**
     * @Author 小帅丶
     * @Description 给图片进行标注
     * @Date  2020年6月17日14:29:38
     * @param imageBytes 图片
     * @param bean 包含坐标的对象 x-left y-top width-width height-height
     * @return String 处理后的base64
     **/
    public static String getReactByWrinkle(byte[] imageBytes, MedicalBeautyWrinkleResponseBean bean) throws Exception{
        ByteArrayInputStream bais = new ByteArrayInputStream(imageBytes);
        BufferedImage image = ImageIO.read(bais);
        Graphics g = image.getGraphics();
        Graphics2D g2d = (Graphics2D) g;
        Font font = new Font("微软雅黑", Font.ITALIC, 16);
        g2d.setStroke(new BasicStroke(3.0f));
        g2d.setColor(Color.RED);
        g2d.setFont(font);
        List<List<MedicalBeautyWrinkleResponseBean.ResultBean.FaceListBean.WrinkleBean.WrinkleDataBean>> wrinkle_data = bean.getResult().getFace_list().get(0).getWrinkle().getWrinkle_data();
        for (int i = 0; i < wrinkle_data.size(); i++) {
            for (int j = 0; j <wrinkle_data.get(i).size() ; j++) {
                List<MedicalBeautyWrinkleResponseBean.ResultBean.FaceListBean.WrinkleBean.WrinkleDataBean> wrinkleDataBeans = wrinkle_data.get(i);
                int j2 = j+1;
                if(j2<wrinkleDataBeans.size()){
                    g2d.drawLine(wrinkleDataBeans.get(j).getX(), wrinkleDataBeans.get(j).getY(), wrinkleDataBeans.get(j2).getX(), wrinkleDataBeans.get(j2).getY());
                }
            }
        }
        g2d.setFont(font);
        g2d.setColor(Color.white);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", outputStream);
        String base64Img = Base64Util.encode(outputStream.toByteArray());
        return base64Img;
    }

    /**
     * @Author 小帅丶
     * @Description 给图片增加框-黑眼圈眼袋
     * @Date  2020年8月26日17:49:05
     * @param imageBytes 图片
     * @param bean 包含坐标的对象 x-left y-top
     * @return void
     **/
    public static String getReactEyesAttr(byte[] imageBytes, MedicalBeautyEyesAttrResponseBean bean) throws Exception{
        long startTime = System.currentTimeMillis();
        ByteArrayInputStream bais = new ByteArrayInputStream(imageBytes);
        BufferedImage image = ImageIO.read(bais);
        Graphics2D g = (Graphics2D) image.getGraphics();
        Font font = new Font("微软雅黑", Font.ITALIC, 16);
        g.setStroke(new BasicStroke(2.5f));
        g.setColor(Color.RED);
        g.setFont(font);
        //左右黑眼圈对象
        List<List<MedicalBeautyEyesAttrResponseBean.ResultBean.FaceListBean.EyesattrBean.DarkCircleLeftBean>> dark_circle_left = bean.getResult().getFace_list().get(0).getEyesattr().getDark_circle_left();
        List<List<MedicalBeautyEyesAttrResponseBean.ResultBean.FaceListBean.EyesattrBean.DarkCircleRightBean>> dark_circle_right = bean.getResult().getFace_list().get(0).getEyesattr().getDark_circle_right();
        //左右眼袋对象
        List<List<MedicalBeautyEyesAttrResponseBean.ResultBean.FaceListBean.EyesattrBean.EyeBagsLeftBean>> eye_bags_left = bean.getResult().getFace_list().get(0).getEyesattr().getEye_bags_left();
        List<List<MedicalBeautyEyesAttrResponseBean.ResultBean.FaceListBean.EyesattrBean.EyeBagsRightBean>> eye_bags_right = bean.getResult().getFace_list().get(0).getEyesattr().getEye_bags_right();
        //左黑眼圈
        for (int i = 0; i < dark_circle_left.size(); i++) {
            for (int j = 0; j <dark_circle_left.get(i).size() ; j++) {
                List<MedicalBeautyEyesAttrResponseBean.ResultBean.FaceListBean.EyesattrBean.DarkCircleLeftBean> darkCircleLeftBeans = dark_circle_left.get(i);
                int j2 = j+1;
                if(j2<darkCircleLeftBeans.size()){
                    g.drawLine(darkCircleLeftBeans.get(j).getX(), darkCircleLeftBeans.get(j).getY(), darkCircleLeftBeans.get(j2).getX(), darkCircleLeftBeans.get(j2).getY());
                }
            }
        }
        //右黑眼圈
        for (int i = 0; i < dark_circle_right.size(); i++) {
            for (int j = 0; j <dark_circle_right.get(i).size() ; j++) {
                List<MedicalBeautyEyesAttrResponseBean.ResultBean.FaceListBean.EyesattrBean.DarkCircleRightBean> darkCircleRightBeans = dark_circle_right.get(i);
                int j2 = j+1;
                if(j2<darkCircleRightBeans.size()){
                    g.drawLine(darkCircleRightBeans.get(j).getX(), darkCircleRightBeans.get(j).getY(), darkCircleRightBeans.get(j2).getX(), darkCircleRightBeans.get(j2).getY());
                }
            }
        }
        //左眼袋
        for (int i = 0; i < eye_bags_left.size(); i++) {
            for (int j = 0; j <eye_bags_left.get(i).size() ; j++) {
                List<MedicalBeautyEyesAttrResponseBean.ResultBean.FaceListBean.EyesattrBean.EyeBagsLeftBean> eyeBagsLeftBeans = eye_bags_left.get(i);
                int j2 = j+1;
                if(j2<eyeBagsLeftBeans.size()){
                    g.drawLine(eyeBagsLeftBeans.get(j).getX(), eyeBagsLeftBeans.get(j).getY(), eyeBagsLeftBeans.get(j2).getX(), eyeBagsLeftBeans.get(j2).getY());
                }
            }
        }
        //右眼袋
        for (int i = 0; i < eye_bags_right.size(); i++) {
            for (int j = 0; j <eye_bags_right.get(i).size() ; j++) {
                List<MedicalBeautyEyesAttrResponseBean.ResultBean.FaceListBean.EyesattrBean.EyeBagsRightBean> eyeBagsRightBeans = eye_bags_right.get(i);
                int j2 = j+1;
                if(j2<eyeBagsRightBeans.size()){
                    g.drawLine(eyeBagsRightBeans.get(j).getX(), eyeBagsRightBeans.get(j).getY(), eyeBagsRightBeans.get(j2).getX(), eyeBagsRightBeans.get(j2).getY());
                }
            }
        }
        g.setFont(font);
        g.setColor(Color.white);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", outputStream);
        String base64Img = Base64Util.encode(outputStream.toByteArray());
        long endTime = System.currentTimeMillis();
        System.out.println("画框耗时:"+(endTime-startTime));
        return base64Img;
    }

    /**
     * @Author 小帅丶
     * @Description 给图片增加框
     * @Date  2020年4月16日17:53:50
     * @param bytes 图片
     * @param bean 包含坐标的对象 x-left y-top width-width height-height
     * @return String 处理后的base64
     **/
    public static String getReactBySkinColor(byte[] bytes, MedicalBeautySkinColorResponse bean) throws Exception{
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
        BufferedImage image = ImageIO.read(bais);
        Graphics g = image.getGraphics();
        Graphics2D g2d = (Graphics2D) g;
        Font font = new Font("微软雅黑", Font.ITALIC, 16);
        if (bean.getResult().getFace_num()>0) {
            for (int i = 0; i < bean.getResult().getFace_list().size(); i++) {
                int left = (int)bean.getResult().getFace_list().get(i).getLocation().getLeft();
                int top = (int)bean.getResult().getFace_list().get(i).getLocation().getTop();
                int width = bean.getResult().getFace_list().get(i).getLocation().getWidth();
                int height = bean.getResult().getFace_list().get(i).getLocation().getHeight();
                g2d.setColor(new Color(0, 129, 255));
                g2d.setStroke(new BasicStroke(2.0f));
                g2d.drawRect(left,top,width,height);
            }
        }
        g2d.setFont(font);
        g2d.setColor(Color.white);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", outputStream);
        String base64Img = Base64Util.encode(outputStream.toByteArray());
        return base64Img;
    }

    /**
     * @Author 小帅丶
     * @Description 给图片增加框
     * @Date  2020年4月16日17:53:50
     * @param bytes 图片
     * @param bean 包含坐标的对象 x-left y-top width-width height-height
     * @return String 处理后的base64
     **/
    public static String getReact(byte[] bytes,ImageClassifyGeneralResponse bean) throws Exception{
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
        BufferedImage image = ImageIO.read(bais);
        Graphics g = image.getGraphics();
        Graphics2D g2d = (Graphics2D) g;
        Font font = new Font("微软雅黑", Font.ITALIC, 16);
        int left = (int)bean.getLocation_result().getLeft();
        int top = (int)bean.getLocation_result().getTop();
        int width = (int)bean.getLocation_result().getWidth();
        int height = (int)bean.getLocation_result().getHeight();
        g2d.setColor(new Color(0, 129, 255));
        g2d.setStroke(new BasicStroke(3.0f));
        g2d.drawRect(left,top,width,height);
        g2d.setFont(font);
        g2d.setColor(Color.white);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", outputStream);
        String base64Img = Base64Util.encode(outputStream.toByteArray());
        return base64Img;
    }

    /**
     * @Author 小帅丶
     * @Description 给图片增加框
     * @Date  2020/2/27 13:11
     * @param image 图片文件
     * @return String
     **/
    public static String getLine(BufferedImage image, PalmPollingXYBean pollingBean) throws Exception{
        Graphics2D g = (Graphics2D) image.getGraphics();
        Font font = new Font("微软雅黑", Font.BOLD, 16);
        //WL 智慧
        g.setStroke(new BasicStroke(3.0f));
        g.setColor(Color.RED);
        g.setFont(font);
        for (int i = 0; i < pollingBean.getData().getLines().getWL().getX().size(); i++) {
            int i2 = i+1;
            if(i2<pollingBean.getData().getLines().getWL().getX().size()){
                g.drawLine(pollingBean.getData().getLines().getWL().getX().get(i), pollingBean.getData().getLines().getWL().getY().get(i), pollingBean.getData().getLines().getWL().getX().get(i2), pollingBean.getData().getLines().getWL().getY().get(i2));
            }
        }
        //AL 感情
        g.setStroke(new BasicStroke(3.0f));
        g.setColor(Color.GREEN);
        g.setFont(font);
        for (int i = 0; i < pollingBean.getData().getLines().getAL().getX().size(); i++) {
            int i2 = i+1;
            if(i2<pollingBean.getData().getLines().getAL().getX().size()){
                g.drawLine(pollingBean.getData().getLines().getAL().getX().get(i), pollingBean.getData().getLines().getAL().getY().get(i), pollingBean.getData().getLines().getAL().getX().get(i2), pollingBean.getData().getLines().getAL().getY().get(i2));
            }
        }
        //LL 生命
        g.setStroke(new BasicStroke(3.0f));
        g.setColor(Color.ORANGE);
        g.setFont(font);
        for (int i = 0; i < pollingBean.getData().getLines().getLL().getX().size(); i++) {
            int i2 = i+1;
            if(i2<pollingBean.getData().getLines().getLL().getX().size()){
                g.drawLine(pollingBean.getData().getLines().getLL().getX().get(i), pollingBean.getData().getLines().getLL().getY().get(i), pollingBean.getData().getLines().getLL().getX().get(i2), pollingBean.getData().getLines().getLL().getY().get(i2));
            }
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", outputStream);
        String base64Img = Base64Util.encode(outputStream.toByteArray());
        return base64Img;
    }


    /**
     * @Author 小帅丶
     * @Description 给图片增加框
     * @Date  2020/2/27 13:11
     * @param image 图片文件
     * @return void
     **/
    public static Map<String,Object> getLine(BufferedImage image, WSPalmBean pollingBean) throws Exception{
        Map<String,Object> map = new HashMap<>();
        Graphics2D g = (Graphics2D) image.getGraphics();
        Font font = new Font("微软雅黑", Font.BOLD, 16);

        WSPalmPollingXYBean bean = new WSPalmPollingXYBean();
        WSPalmPollingXYBean.LinesBean linesBean = new WSPalmPollingXYBean.LinesBean();

        List<List<Integer>> label1 = pollingBean.getData().getLabels().getLabel1().get(0);
        WSPalmPollingXYBean.LinesBean.ALBean labelALXY = (WSPalmPollingXYBean.LinesBean.ALBean) getLabelXY(label1, "AL");

        linesBean.setAL(labelALXY);

        List<Integer> label1x = labelALXY.getX();
        List<Integer> label1y = labelALXY.getY();

        List<List<Integer>> label3 = pollingBean.getData().getLabels().getLabel3().get(0);
        WSPalmPollingXYBean.LinesBean.WLBean labelWLXY = (WSPalmPollingXYBean.LinesBean.WLBean) getLabelXY(label3, "WL");

        linesBean.setWL(labelWLXY);

        List<Integer> label3x = labelWLXY.getX();
        List<Integer> label3y = labelWLXY.getY();

        List<List<Integer>> label5 = pollingBean.getData().getLabels().getLabel5().get(0);
        WSPalmPollingXYBean.LinesBean.LLBean labelLLXY = (WSPalmPollingXYBean.LinesBean.LLBean) getLabelXY(label5, "LL");

        linesBean.setLL(labelLLXY);

        List<Integer> label5x = labelLLXY.getX();
        List<Integer> label5y = labelLLXY.getY();

        List<List<Integer>> label7 = pollingBean.getData().getLabels().getLabel7().get(0);
        WSPalmPollingXYBean.LinesBean.CLBean labelCLXY = (WSPalmPollingXYBean.LinesBean.CLBean) getLabelXY(label7, "CL");

        linesBean.setCL(labelCLXY);

        List<Integer> label7x = labelCLXY.getX();
        List<Integer> label7y = labelCLXY.getY();

        //智慧线
        g.setStroke(new BasicStroke(3.0f));
        g.setColor(new Color(133,54,245));
        g.setFont(font);
        paintLines(g,label3x,label3y);
        //生命线
        g.setStroke(new BasicStroke(3.0f));
        g.setColor(new Color(91,215,219));
        g.setFont(font);
        paintLines(g,label5x,label5y);
        //事业线
        g.setStroke(new BasicStroke(3.0f));
        g.setColor(new Color(61,55,243));
        g.setFont(font);
        paintLines(g,label7x,label7y);
        //感情线
        g.setStroke(new BasicStroke(3.0f));
        g.setColor(new Color(215,79,225));
        g.setFont(font);
        paintLines(g,label1x,label1y);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(image, "png", outputStream);
        String base64Img = Base64Util.encode(outputStream.toByteArray());
        bean.setLines(linesBean);
        map.put("image", base64Img);
        map.put("lines", bean);
        return map;
    }
    /**
     * @Author 小帅丶
     * @Description 给图片画线
     * @Date  2020/5/8 16:48
     * @param g 平面对象
     * @param labelx x坐标对象
     * @param labely y坐标哦对象
     * @return void
     **/
    private static void paintLines(Graphics2D g, List<Integer> labelx,List<Integer> labely) {
        for (int i = 0; i < labelx.size(); i++) {
            int i2 = i+1;
            if(i2<labelx.size()){
                g.drawLine(labelx.get(i), labely.get(i),labelx.get(i2), labely.get(i2));
            }
        }
    }

    /**
     * @Author 小帅丶
     * @Description 获取线的坐标
     * @Date  2020/5/8 16:16
     * @Param [label1]
     * @return cn.ydxiaoshuai.tools.vo.WSPalmPollingXYBean.LinesBean.ALBean
     **/
    private static Object getLabelXY(List<List<Integer>> label,String type) {
        Integer label1X[] = new Integer[label.size()];
        Integer label1y[] = new Integer[label.size()];
        for (int i = 0; i < label.size(); i++) {
            label1X[i] = label.get(i).get(0);
            label1y[i] = label.get(i).get(1);
        }
        List<Integer> collectX = Arrays.stream(label1X).collect(Collectors.toList());
        List<Integer> collectY = Arrays.stream(label1y).collect(Collectors.toList());
        if(type.equals("AL")){
            WSPalmPollingXYBean.LinesBean.ALBean bean = new WSPalmPollingXYBean.LinesBean.ALBean();
            bean.setX(collectX);
            bean.setY(collectY);
            return bean;
        }
        if(type.equals("WL")){
            WSPalmPollingXYBean.LinesBean.WLBean bean = new WSPalmPollingXYBean.LinesBean.WLBean();
            bean.setX(collectX);
            bean.setY(collectY);
            return bean;
        }
        if(type.equals("LL")){
            WSPalmPollingXYBean.LinesBean.LLBean bean = new WSPalmPollingXYBean.LinesBean.LLBean();
            bean.setX(collectX);
            bean.setY(collectY);
            return bean;
        }
        if(type.equals("CL")){
            WSPalmPollingXYBean.LinesBean.CLBean bean = new WSPalmPollingXYBean.LinesBean.CLBean();
            bean.setX(collectX);
            bean.setY(collectY);
            return bean;
        }
        return  null;
    }
}
